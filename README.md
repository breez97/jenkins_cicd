# Лабораторная 2 <br> Jenkins CI/CD

## Цель: обучиться работе с Jenkins

Развернуть Jenkins на виртуальной машине с Ubuntu/Debian. 
Создать проект, либо найти и форкнуть репозиторий на любом компилируемом языке программирования (C, C++, C#, Java, Rust…). Далее, создать в нём файл Jenkinsfile с описанием пайплайна. Данный пайплайн должен собирать проект и выдавать в качестве результата артефакт - готовый бинарник.

### Требования:

 - Jenkinsfile должен подгружаться из репозитория с проектом.
 - Сборка проекта должна происходить внутри соответствующего докер-контейнера.
 - При успешной работе пайплайна выгружать готовый бинарник как артефакт.

### Содержание:

0. [**Развернуть Jenkins на Ubuntu/Debian.**](#развертывание-jenkins-на-ubuntu)
1. [**Создать или выбрать репозиторий с проектом.**](#задание-1)
2. [**Создать Jenkinsfile с описанием пайплайна.**](#задание-2)
3. [**Получение артефакта выполнения.**](#задание-3)

### Развертывание Jenkins на Ubuntu:

![](./misc/Installation/install_jenkins_docker.png)
![](./misc/Installation/jenkins_dashboard.png)

## Задание 1:

Создание или выбор репозитория проекта.

Результатом выполнения будет ссылка на репозиторий.

Для выполнения данной практической работы используется проект с собстенной реализацией утилиты командной строки `grep` на языке `C`.

Пример работы программы:

![](./misc/Task_1/example.png)

### Результат:

#### <a href="https://gitlab.com/breez97/jenkins_cicd">Ссылка на репозиторий</a>

## Задание 2:

Создать `Jenkinsfile` с описанием пайплайна.

Результатом будет содержимое пайплайна в описании задачи.

Поскольку сборка проекта должна выполняться внутри соответсвующего `docker` контейнера, то для этого был использован `agent` с необходимым образом `gcc:latest`, который содержит в себе все необходимый инструменты для билда и запуска программы на языке `C`.

Пайплайн содержит в себе следующие этапы:

1. Сборка проекта внутри `docker` контейнера -> `stage('Build')`.
2. Проверка на стиль кода с помощью утилиты `clang-format` -> `stage('Check Style')`.
3. Проверка работоспособности программы с помощью `sh` скрипта `test_grep_func.sh`, в котором прописаны тесты для сравнения результатов работы стандартной утилиты `grep` с результатами собственной реализации -> `stage('Test')`.
4. Выгрузка артефактов работы (результирующий `executable` бинарный файл: `program`) -> `post`.

### Результат:

```
pipeline {
	agent {
		docker 'gcc:latest'
	}
	
	stages {
		stage('Build') {
			steps {
				echo 'Building project...'
				sh 'mkdir -p build'
				sh 'gcc -Wall -Werror -Wextra src/support_functions/*.c src/grep.c -o build/program'
			}
		}
		stage('Check Style') {
			steps {
				echo 'Checking style of the code...'
				sh 'apt update && apt install -y clang-format'
				sh 'clang-format -n src/support_functions/*.c src/grep.c src/*.h'
			}
		}
		stage('Test') {
			steps {
				echo 'Test project...'
				sh 'build/program'
				echo 'Use Tests...'
				sh 'cp src/test_grep_func.sh build/'
				sh 'build/test_grep_func.sh'
			}
		}
	}

	post {
		success {
			archiveArtifacts artifacts: 'build/program'
		}
	}
}
```

#### Скриншот выполнения пайплайна:

![](./misc/Task_2/jenkins_pipeline_schema.png)

#### Скриншот логов:

![](./misc/Task_2/jenkins_console_output_1.png)
![](./misc/Task_2/jenkins_console_output_2.png)

## Задание 3:

Получить артефакт выполнения.

Результатом задачи будет артефакт сборки. Артефактом сборки должен быть именно исполняемый файл (`.exe`, `.jar` и другие).

В данном случае после выполнения пайплана будет получен исполняемый файл `program`.

### Результат:

#### <a href="https://media-protected.taiga.io/attachments/4/4/9/3/f6d57d0e9723646e96cfa4673bf216dbaf4c5ff79d1a63530d5e3a7ad313/program?token=ZfyJmA%3AwFG0r1AyP-JZqOrrZbPU79d_Mm53lJ7knRjXgrXtBtibL0G1PTeTCF7uMQEgqSOMQDWga2u8ogbTK1a_A-RAuw#_taiga-refresh=task:2309445">Исполняемый файл</a>

![](./misc/Task_3/artifact_result.png)
