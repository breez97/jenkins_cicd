#include "grep.h"

int main(int argc, char *argv[]) {
  ProgramStart(argc, argv);
  return 0;
}

void ProgramStart(int argc, char *argv[]) {
  flags inputFlags = {0};

  char **patternsArray = NULL;
  int countPatterns = 0;

  regex_t *regexes = NULL;
  int countRegexes = 0;

  char **files = NULL;
  int countFiles = 0;

  if (FlagsAndFiles(argc, argv, &inputFlags, &patternsArray, &countPatterns) ==
          1 ||
      argc == 1) {
    fprintf(stderr, "Usage: grep [OPTION]... PATTERN [FILE]...\n");
  } else {
    ConvertRegexes(inputFlags, patternsArray, countPatterns, &regexes,
                   &countRegexes);

    FindFiles(inputFlags, argc, argv, &files, &countFiles);

    SearchInFile(inputFlags, regexes, countRegexes, files, countFiles);
  }

  for (int i = 0; i < countRegexes; i++) regfree(&regexes[i]);
  for (int i = 0; i < countFiles; i++) free(files[i]);
  for (int i = 0; i < countPatterns; i++) free(patternsArray[i]);
  free(patternsArray);
  free(files);
  free(regexes);
}
