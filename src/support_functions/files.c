#include "../grep.h"

void SearchInFile(flags inputFlags, regex_t *regexes, int countRegexes,
                  char **files, int countFiles) {
  FILE *file;
  char *filename = NULL;
  if (countFiles == 0) {
    file = freopen(NULL, "rb", stdin);
    OutputFile(filename, file, inputFlags, regexes, countRegexes, countFiles);
  } else {
    for (int i = 0; i < countFiles; i++) {
      file = fopen(files[i], "rb");
      if (file == NULL) {
        if (inputFlags.s == 0)
          fprintf(stderr, "grep: %s: No such file or directory\n", files[i]);
      } else
        OutputFile(files[i], file, inputFlags, regexes, countRegexes,
                   countFiles);
      if (file != NULL) fclose(file);
    }
  }
}

void OutputFile(char *filename, FILE *file, flags inputFlags, regex_t *regexes,
                int countRegexes, int countFiles) {
  char *line = NULL;
  size_t len = 0;
  int countString = 0, isSuitableFile = 0, numberLine = 1;
  while (getline(&line, &len, file) != -1) {
    int isLinePrinted = 0, suitable = 0;
    line[strcspn(line, "\n")] = '\0';
    for (int i = 0; i < countRegexes; i++) {
      if (regexec(&regexes[i], line, 0, NULL, 0) == 0) suitable = 1;
    }
    if (inputFlags.v == 0) {
      PrintIsSuitable(suitable, inputFlags, countFiles, isLinePrinted, filename,
                      line, numberLine, regexes, countRegexes, &countString,
                      &isSuitableFile);
    } else {
      PrintIsNotSuitable(suitable, inputFlags, countFiles, isLinePrinted,
                         filename, numberLine, line, &countString,
                         &isSuitableFile);
    }
    numberLine++;
  }
  if (inputFlags.c == 1) {
    if (countFiles > 1 && inputFlags.h == 0) printf("%s:", filename);
    if (inputFlags.l == 1 && countString > 0) countString = 1;
    printf("%d\n", countString);
  }
  if (inputFlags.l == 1 && isSuitableFile) printf("%s\n", filename);
  free(line);
}
