#include "../grep.h"

int FlagsAndFiles(int argc, char *argv[], flags *inputFlags,
                  char ***patternsArray, int *countPatterns) {
  int opt;
  int isIllegalOption = 0;
  while (isIllegalOption == 0 &&
         (opt = getopt_long(argc, argv, "e:ivclnhsf:o", NULL, NULL)) != -1) {
    switch (opt) {
      case 'e':
        inputFlags->e = 1;
        FillPatternsArray(optarg, patternsArray, countPatterns);
        break;
      case 'i':
        inputFlags->i = 1;
        break;
      case 'v':
        inputFlags->v = 1;
        break;
      case 'c':
        inputFlags->c = 1;
        break;
      case 'l':
        inputFlags->l = 1;
        break;
      case 'n':
        inputFlags->n = 1;
        break;
      case 'h':
        inputFlags->h = 1;
        break;
      case 's':
        inputFlags->s = 1;
        break;
      case 'f':
        inputFlags->f = 1;
        GetPatternsFromFile(optarg, patternsArray, countPatterns);
        break;
      case 'o':
        inputFlags->o = 1;
        break;
      default:
        isIllegalOption = 1;
        break;
    }
  }
  if (inputFlags->e == 0 && inputFlags->f == 0) {
    AddPatternIsSingleFlag(argc, &isIllegalOption, argv[optind], patternsArray,
                           countPatterns);
  }
  return isIllegalOption;
}

void AddPatternIsSingleFlag(int argc, int *isIllegalOption, char *pattern,
                            char ***patternsArray, int *countPatterns) {
  if (optind == argc) *isIllegalOption = 1;
  if (optind < argc) FillPatternsArray(pattern, patternsArray, countPatterns);
}

void FindFiles(flags inputFlags, int argc, char *argv[], char ***files,
               int *countFiles) {
  if (inputFlags.e == 0) optind += 1;
  if (inputFlags.f == 1) optind -= 1;
  if (inputFlags.e == 1 && inputFlags.f == 1) optind += 1;
  for (int i = optind; i < argc; i++) {
    (*countFiles) += 1;
    (*files) = realloc(*files, sizeof(char *) * (*countFiles));
    (*files)[(*countFiles) - 1] = malloc(sizeof(char) * (strlen(argv[i]) + 1));
    strcpy((*files)[(*countFiles) - 1], argv[i]);
  }
}
