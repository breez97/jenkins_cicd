#include "../grep.h"

void GetPatternsFromFile(char *filename, char ***patternsArray,
                         int *countPatterns) {
  FILE *file = fopen(filename, "r");

  if (file == NULL) {
    fprintf(stderr, "grep: %s: No such file or directory\n", filename);
  } else {
    char *line = NULL;
    size_t len;
    while (getline(&line, &len, file) != -1) {
      line[strcspn(line, "\n")] = '\0';
      FillPatternsArray(line, patternsArray, countPatterns);
    }
    free(line);
  }
  fclose(file);
}

void FillPatternsArray(char *pattern, char ***patternsArray,
                       int *countPatterns) {
  (*countPatterns) += 1;
  (*patternsArray) = realloc(*patternsArray, sizeof(char *) * (*countPatterns));
  (*patternsArray)[(*countPatterns) - 1] =
      malloc(sizeof(char) * (strlen(pattern) + 1));
  strcpy((*patternsArray)[(*countPatterns) - 1], pattern);
}
