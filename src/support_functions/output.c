#include "../grep.h"

void ConvertRegexes(flags inputFlags, char **patternsArray, int countPatterns,
                    regex_t **regexes, int *countRegexes) {
  for (int i = 0; i < countPatterns; i++) {
    regex_t *regex = malloc(sizeof(regex_t));
    (*countRegexes) += 1;
    *regexes = realloc(*regexes, sizeof(regex_t) * (*countRegexes));
    if (inputFlags.i == 1)
      regcomp(regex, patternsArray[i], REG_ICASE);
    else
      regcomp(regex, patternsArray[i], REG_EXTENDED);
    (*regexes)[(*countRegexes) - 1] = *regex;
    free(regex);
  }
}

void PrintIsSuitable(int suitable, flags inputFlags, int countFiles,
                     int isLinePrinted, char *filename, char *line,
                     int numberLine, regex_t *regexes, int countRegexes,
                     int *countString, int *isSuitableFile) {
  if (suitable == 1) {
    if (inputFlags.c == 0 && inputFlags.l == 0) {
      if (countFiles > 1 && inputFlags.h == 0 && isLinePrinted == 0)
        printf("%s:", filename);
      if (inputFlags.n == 1) printf("%d:", numberLine);
      if (inputFlags.o == 1)
        PrintFlagO(regexes, line, countRegexes);
      else
        printf("%s\n", line);
    }
    (*countString)++;
    *isSuitableFile = 1;
  }
}

void PrintIsNotSuitable(int suitable, flags inputFlags, int countFiles,
                        int isLinePrinted, char *filename, int numberLine,
                        char *line, int *countString, int *isSuitableFile) {
  if (suitable == 0) {
    if (inputFlags.c == 0 && inputFlags.l == 0) {
      if (countFiles > 1 && inputFlags.h == 0 && isLinePrinted == 0)
        printf("%s:", filename);
      if (inputFlags.n == 1) printf("%d:", numberLine);
      printf("%s\n", line);
    }
    (*countString)++;
    *isSuitableFile = 1;
  }
}

void PrintFlagO(regex_t *regexes, char *line, int countRegexes) {
  int currentPos = 0;
  for (int i = 0; i < countRegexes; i++) {
    regmatch_t matches[1];
    while (regexec(&regexes[i], line + currentPos, 1, matches, 0) == 0) {
      int start = currentPos + matches[0].rm_so;
      int end = currentPos + matches[0].rm_eo;
      for (int j = start; j < end; j++) printf("%c", line[j]);
      printf("\n");
      currentPos += matches[0].rm_eo;
    }
  }
}
